require 'sinatra'
require 'ex_libris_api'
require 'rest-client'
require 'nori'
require "sinatra/config_file"

config_file 'config.yml'

configure do
  api = ExLibrisAPI::API.new(
    api_key: settings.api_key,
    format: 'application/json')

  set :api, api
  set :sru_uri, settings.sru_uri # Krascha tidigt ifall den saknas i config.yml
  set :xml_parser, Nori.new

  optional_settings_defaults = {
    policies: {},
    processes: {},
    location_policies: {},
    fu_location_policies: {},
    fulfillment_units: {},
    fulfillment_unit_policies: {},
  }

  optional_settings_defaults.each do |opt, default|
    set opt, default unless settings.respond_to?(opt)
  end

  # Tillåt en färdig mappning med location => policy
  if settings.fu_location_policies.empty?

    # Använd fulfillment_units och fulfillment_unit_policies för att konstruera
    # mappningen location => policy
    set :fu_location_policies, settings.fulfillment_units
      .map { |fu_code, fu_locs|
        fu_locs.map { |loc| [loc, settings.fulfillment_unit_policies[fu_code]] }
      }
      .flatten(1)
      .reject { |_,policy| policy.nil? }
      .to_h

  end

end

get '/?' do
  bib_id = params['bib_id']
  library = params['library']
  halt 400 unless bib_id && library

  # Hämta posten via SRU med uppslag på Librisnumret.
  sru_response = RestClient.get(
    settings.sru_uri +
    '?version=1.2&operation=searchRetrieve&maximumRecords=1&query=alma.other_system_number=%28LIBRIS%29' +
    bib_id.to_s
  )

  # Extrahera MMS-ID från posten.
  records = settings.xml_parser.parse(sru_response)['searchRetrieveResponse']['records']
  halt 404 unless records
  record = [records['record']][0]['recordData']['record']
  control_fields = [*record['controlfield']]
  mms_id = control_fields.find { |cf| cf.attributes['tag'] == '001' }

  # Hämta exemplardata via Almas API
  items = settings.api.x_get_items(
    mms_ids: mms_id,
    sort_by: 'base_status,location,call_number,policy',
    reverse: 'base_status',
    group_by: 'library',
  )
  halt 404 if items.empty?

  items_xml = ''
  items[library].each_with_index do |item, i|
    status = item[:base_status] == 0 ? 'Ej tillgänglig' : 'Tillgänglig'
    due_date = ''

    if item[:process_type] == 'LOAN'
      status = 'Utlånad'
      due_date = item[:due_date][0..9]
    elsif settings.processes[item[:process_type]]
      status = settings.processes[item[:process_type]]
    end

    policy = settings.location_policies.fetch(item[:location]) {
      settings.policies.fetch(item[:policy]) {
        settings.fu_location_policies[item[:location]]
      }
    }

    items_xml +=
      '<Item>'+
      "<Item_No>#{(i+1).to_s}</Item_No>"+
      "<UniqueItemId>#{item[:barcode]}</UniqueItemId>"+
      "<Location>#{item[:location_desc]}</Location>"+
      "<Call_No>#{item[:call_number]}</Call_No>"+
      "<Loan_Policy>#{policy}</Loan_Policy>"+
      "<Status>#{status}</Status>"+
      "<Status_Date>#{due_date}</Status_Date>"+
      '<Status_Date_Description />'+
      '</Item>'
  end

  content_type :xml, charset: 'utf-8'

  '<?xml version="1.0" encoding="UTF-8"?>'+
  '<Item_Information xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="http://appl.libris.kb.se/LIBRISItem.xsd">'+
  items_xml+
  '</Item_Information>'
end
